package Book;
import Entity.Entity;
public class Book extends Entity{
    @Override
    public String toString() {
        return "Book{" +
                "status=" + status +
                ", dueDateYet=" + dueDateYet +
                ", id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public Availability getStatus() {
        return status;
    }

    public void setStatus(Availability status) {
        this.status = status;
    }

    public boolean isDueDateYet() {
        return dueDateYet;
    }

    public void setDueDateYet(boolean dueDateYet) {
        this.dueDateYet = dueDateYet;
    }



    Availability status;
    boolean dueDateYet;
    public Book (long libraryNumber,String name,Availability status) {
        this.name=name;
        this.id=libraryNumber;
        this.status=status;
        this.dueDateYet=false;
    }
}
