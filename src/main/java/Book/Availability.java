package Book;
public enum Availability {
    AVAILABLE, BEING_DELIVERED, BORROWED
}