package Member;

public enum Sex {
    MALE, FEMALE, UNKNOWN;

    public String toString() {
        if (this.equals(Sex.MALE)) {
            return new String("male");
        } else if (this.equals(Sex.FEMALE)) {
            return new String("female");
        }
        return new String("invalid");
    }
}