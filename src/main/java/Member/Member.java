package Member;

import Book.Book;
import Entity.Entity;
import java.util.Arrays;


public class Member extends Person {
    @Override
    public String toString() {
        return "Member{" +
                "age=" + age +
                ", sex=" + sex +
                ", borrowedBooks=" + Arrays.toString(borrowedBooks) +
                ", id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public boolean equals(Object otherObject) {
        if(otherObject instanceof Member) {
            Member other=(Member)otherObject;
            if(this.name.equals(other.name)&&this.age==other.age&&this.id==other.id&&this.sex.equals(other.sex)) {
                return true;
            }
        }
        return false;
    }



    public Member(long registrationCode, String name, int age, Sex sex) {
        this.id = registrationCode;
        this.name = name;
        this.age = age;
        this.sex = sex;
    }



    private Book[] borrowedBooks;

}