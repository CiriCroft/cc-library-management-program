package Member;
import Member.Sex;
import Entity.Entity;
public class Person extends Entity {
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    protected int age;
    protected Sex sex;
}
