
import java.util.Scanner;
import java.util.Vector;

import Member.Member;
import Book.Book;
import Member.Sex;
import Book.Availability;
import Entity.Entity;


public class Main {

    public static void main(String[] args) {
        UI userInterface = new UI();
        try {
            userInterface.start();
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

    }

}


class UI {
    Library library;
    private final int pageLines = 40;

    private void clearPage() {
        for (int i = 0; i < pageLines; i++) {
            System.out.println("\n");
        }
    }

    private void welcome() {
        System.out.println("Welcome to CC library management program \n this application was developed by Ali Zarean aka ciricroft \n");
    }

    private void goodBye() {
        System.out.println("THANKS FOR USING OUR APP!");
    }

    Sex getSexFromString(String sex) {
        if (sex.toLowerCase().equals("f")) {
            return Sex.FEMALE;
        } else if (sex.toLowerCase().equals("m")) {
            return Sex.MALE;
        } else {
            return Sex.UNKNOWN;
        }
    }

    void addMember() {
        Scanner input = new Scanner(System.in);
        clearPage();
        System.out.println("please enter the properties of new member in this order: Registration number - name - age - sex(f for female and m for male)");
        long registrationNumber = input.nextLong();
        String name = input.next();
        int age = input.nextInt();
        Sex sex = getSexFromString(input.next());
        try {
            library.addMember(registrationNumber, name, age, sex);
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }


        clearPage();
        System.out.println("done \n");

    }

    void deleteMember() {
        Scanner input = new Scanner(System.in);
        clearPage();
        long RegistrationNumber = input.nextLong();
        if (library.deleteMember(RegistrationNumber)) {
            System.out.println("Done\n");
        } else {
            System.out.println("couldnt find a member with that registration number\n");
        }
    }

    void searchMemberByName() {
        Scanner input = new Scanner(System.in);
        String name = input.next();
        Vector<Member> foundMembers = library.searchByName(name);
        if (foundMembers.size() == 0) {
            System.out.println("no such members where found. check your spelling please");
            return;
        }
        for (Member member : foundMembers) {
            String sex = member.toString();
            System.out.println("name: " + member.getName() + "\t" + "age: " + member.getAge() + "\t" + "sex: " + sex + "\t registration number: " + member.getId() + "\n");
        }
    }

    void searchMemberById() {
        Scanner input = new Scanner(System.in);
        long registrationNumber = input.nextLong();
        Member member = library.searchByRegistrationNumber(registrationNumber);
        if (member == null) {
            System.out.println("couldnt find what you were looking for. you dummy!");
            return;
        }
        String sex = member.toString();
        System.out.println("name: " + member.getName() + "\t" + "age: " + member.getAge() + "\t" + "sex: " + sex + "\t registration number: " + member.getId() + "\n");
    }

    void searchMember() {
        clearPage();
        System.out.println("type -id- to search by registration number and type -name- to search by name and provide id or name afterwards");
        Scanner input = new Scanner(System.in);
        String command = input.next();
        if (command.equals("name")) {
            searchMemberByName();
        } else if (command.equals("id")) {
            searchMemberById();
        }
    }

    void editMember() {
        clearPage();
        Scanner input = new Scanner(System.in);
        System.out.println("please enter the new properties in this order: registration number - name - age");
        long registrationNumber = input.nextLong();
        String name = input.next();
        int age = input.nextInt();
        if (library.editMember(registrationNumber, name, age)) {
            System.out.println("DONE\n");
        } else {
            System.out.println("couldnt find a member with such registration number\n");
        }
    }

    void addBook() {
        Scanner input = new Scanner(System.in);
        clearPage();
        System.out.println("please enter the properties of the new book in the following order: library number - name - status (status can be one of these: AVAILABLE, BEING_DELIVERED, BORROWED)");
        long libraryNumber = input.nextLong();
        String name = input.next();
        String stringOfStatus = input.next();
        Availability status = Availability.valueOf(stringOfStatus);
        try {
            library.addBook(libraryNumber, name, status);
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

    }

    void editBook() {
        Scanner input = new Scanner(System.in);
        clearPage();
        System.out.println("please enter the library number of the book you want to edit");
        long id = input.nextLong();
        System.out.println("now enter the properties you want to change in the following order: library number - name - status (status can be one of these: AVAILABLE, BEING_DELIVERED, BORROWED) - due to its return(True or False)");
        String name = input.next();
        String stringOfStatus = input.next();
        Availability status = Availability.valueOf(stringOfStatus);
        String valueOfBool = input.next();
        boolean dueToReturn = Boolean.getBoolean(valueOfBool);
        if (library.editBook(id, name, status, dueToReturn)) {
            clearPage();
            System.out.println("done\n");
        } else {
            System.out.println("FAILED \n");
        }
    }

    void removeBook() {
        Scanner input = new Scanner(System.in);
        System.out.println("enter the id of the book you want to delete");
        long id = input.nextLong();
        if (library.removeBook(id)) {
            System.out.println(id + " removed successfully \n");
        }
    }

    void searchBook() {
        Scanner input = new Scanner(System.in);
        System.out.println("enter the id of the book you want to view");
        long id = input.nextLong();
        clearPage();
        System.out.println(library.searchBook(id));
    }

    public void start() {
        welcome();
        //getting commands
        {
            Scanner input = new Scanner(System.in);
            String command = "";
            String addingMemberCommand = "add_member";
            String deletingMemberCommand = "delete_member";
            String searchingMemberCommand = "search_member";
            String editingMemberCommand = "edit_member";
            String addingBookCommand = "add_book";
            String editingBookCommand = "edit_book";
            String removingBookCommand = "remove_book";
            String searchingBookCommand = "search_book";
            System.out.println("enter the maximum size of library");
            //initializing the library object
            {
                int size = input.nextInt();
                this.library = new Library(size);
            }
            System.out.println("choose what you want to do from the list of available instructions shown below by typing the start of that action name in the console: \n");
            while (!command.toLowerCase().equals("exit")) {
                //printing available instructions
                {
                    System.out.println("add_member: you will provide some basic information about the new member and add it to the library");

                    System.out.println("delete_member: you will provide the registration number of a member and he/she will be removed from library members");

                    System.out.println("search_member: you can search for members by a specific type of property such as name,regnumber,etc...");

                    System.out.println("edit_member: you will provide the registration number of a member so you can change his/her properties");

                    System.out.println("add_book: add a new book");

                    System.out.println("edit_book: edit an existiong book");

                    System.out.println("remove_book: remove an existing book");

                    System.out.println("search_book: search for a book and see its properties");

                    System.out.println("exit: the program will terminate");
                }
                command = input.next();
                if (addingMemberCommand.startsWith(command.toLowerCase())) {
                    this.addMember();
                } else if (deletingMemberCommand.startsWith(command.toLowerCase())) {
                    this.deleteMember();
                } else if (searchingMemberCommand.startsWith(command.toLowerCase())) {
                    this.searchMember();
                } else if (editingMemberCommand.startsWith(command.toLowerCase())) {
                    this.editMember();
                } else if (command.equals("exit")) {
                    break;
                } else if (addingBookCommand.startsWith(command.toLowerCase())) {
                    this.addBook();
                } else if (editingBookCommand.startsWith((command.toLowerCase()))) {
                    this.editBook();
                } else if (searchingBookCommand.startsWith(command.toLowerCase())) {
                    this.searchBook();
                } else if (removingBookCommand.startsWith(command.toLowerCase())) {
                    this.removeBook();
                } else {
                    clearPage();
                    System.out.println("unknown command");
                }
            }
        }
        goodBye();
    }

}


class Library {
    private Member[] members;
    private Book[] books;
    private int sizeOfMembers;
    private int sizeOFBooks;

    public Library(int maxSize) {
        this.members = new Member[maxSize];
        this.sizeOfMembers = 0;
        this.books = new Book[maxSize];
        this.sizeOFBooks = 0;
    }

    void addMember(long registrationCode, String name, int age, Sex sex) throws Exception {
        if (sizeOfMembers == members.length) {
            Exception exception = new Exception();
            throw exception;
        }
        Member newMember = new Member(registrationCode, name, age, sex);
        members[sizeOfMembers] = newMember;
        this.sizeOfMembers += 1;
    }

    Vector<Member> searchByName(String name) {
        Vector<Member> results = new Vector<Member>();
        for (int i = 0; i < sizeOfMembers; i++) {
            Member currentMember = members[i];
            if (currentMember.getName().toLowerCase().equals(name.toLowerCase())) {
                results.add(currentMember);
            }
        }
        return results;
    }

    Member searchByRegistrationNumber(long registrationNumber) {
        for (int i = 0; i < sizeOfMembers; i++) {
            Member currentMember = members[i];
            if (currentMember.getId() == registrationNumber) {
                return currentMember;
            }
        }
        return null;
    }

    boolean deleteMember(long registrationNumber) {
        boolean found = false;
        int index = 0;
        for (int i = 0; i < sizeOfMembers; i++) {
            if (members[i].getId() == registrationNumber) {
                found = true;
                index = i;
                break;
            }
        }
        if (!found) {
            return false;
        }
        for (int i = index; i < sizeOfMembers - 1; i++) {
            members[i] = members[i + 1];
        }
        sizeOfMembers -= 1;
        return true;
    }

    boolean editMember(long registrationNumber, String name, int age) {
        Member wantedMember = null;
        for (int i = 0; i < sizeOfMembers; i++) {
            if (members[i].getId() == registrationNumber) {
                wantedMember = members[i];
            }
        }
        if (wantedMember == null) {
            return false;
        }
        wantedMember.setName(name);
        wantedMember.setAge(age);
        return true;
    }

    void addBook(long id, String name, Availability status) throws Exception {
        if (sizeOFBooks == books.length) {
            throw new Exception();
        }
        Book newBook = new Book(id, name, status);
        this.books[sizeOFBooks] = newBook;
        this.sizeOFBooks++;
    }

    boolean editBook(long id, String name, Availability status, boolean dueDateYet) {
        Book book = null;
        for (int i = 0; i < this.sizeOFBooks; i++) {
            if (books[i].getId() == id) {
                book = books[i];
                break;
            }
        }
        if (book == null) {
            return false;
        }
        book.setName(name);
        book.setStatus(status);
        book.setDueDateYet(dueDateYet);
        return true;
    }

    boolean removeBook(long id) {
        int i = 0;
        while (i < sizeOFBooks) {
            if (books[i].getId() == id) {
                break;
            }
            i++;
        }
        if (i == sizeOFBooks) {
            return false;
        }
        books[i] = null;
        for (; i < sizeOFBooks - 1; i++) {
            books[i] = books[i + 1];
        }
        sizeOFBooks--;
        return true;
    }

    String searchBook(long id) {
        Book book = null;
        for (int i = 0; i < sizeOFBooks; i++) {
            if (books[i].getId() == id) {
                book = books[i];
                break;
            }
        }
        if (book == null) {
            return new String("INVALID");
        }
        return book.toString();
    }

}