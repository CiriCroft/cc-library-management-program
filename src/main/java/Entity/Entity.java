package Entity;

public class Entity {
    public long getId() {
        return id;
    }

    protected long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    protected String name;
}
