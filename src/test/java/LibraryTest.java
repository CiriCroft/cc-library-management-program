import Book.Availability;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import Member.Member;
import Member.Sex;
import java.util.Vector;

class LibraryTest {

    final int sizeOfLibrary=100;
    Library library;

    @BeforeAll
    public void initLibrary() {
        library=new Library(sizeOfLibrary);
    }

    @Test
    public void testSearchByName() {
        Vector<Member>mamadMembers=new Vector<Member>();
        for (int i=0;i<100;i++) {
            mamadMembers.add(i,new Member(i,"mamad",20,Sex.MALE));
        }


        for(Member member:mamadMembers) {
            try {
                library.addMember(member.getId(),member.getName(),member.getAge(),member.getSex());
            }
            catch (Exception exception) {
                mamadMembers=new Vector<Member>();
                break;
            }
        }

        assertEquals(library.searchByName("mamad"),mamadMembers);



    }

    @Test
    public void testEditMember() {
        try {
            library.addMember(100,"mamad",20,Sex.MALE);
        }
        catch(Exception exception) {
            assertNotEquals(null,exception);
        }

        assertTrue(library.editMember(100,"Ali",20));

    }

    @Test
    public void testRemoveBook() {
        try {
            library.addBook(10,"testbook", Availability.AVAILABLE);
        }
        catch (Exception exception) {
            assertNotEquals(null,exception);
        }
        assertTrue(library.removeBook(10));
    }
}